$(function(){

    //  COMPLETING TASK
    $('.my-task-to-complete').on('click', function (e){
        e.preventDefault();

        var thisTask = $(this);
        var taskId = thisTask.val();
        
        $.ajax({
           
            type: "POST",
            url: "completeTask",
            
            data: {
                id: taskId
            },
            
            success: function() {
                
                var date = new Date();
                var today = ("0" + date.getDate()).slice(-2) 
                        + "/" + ("0" + (date.getMonth() + 1)).slice(-2) 
                        + "/" + date.getFullYear();

                thisTask.hide();
                $("#status-of-task-" + taskId).text("Complete");
                $("#date-of-achievement-for-task-" + taskId).text(today);

                $('#my-notice').text('Task ' + taskId + ' has successfully been completed.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');
            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with completion of task ' + taskId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });


    //  DELETING TASK
    $('.my-task-to-delete').on('click', function (e){
        e.preventDefault();

        var thisTask = $(this);
        var taskId = thisTask.val();

        $.post("deleteTask", {'id' : taskId}, function() {

            

        });
        
        $.ajax({
           
            type: "POST",
            url: "deleteTask",
            
            data: {
                id: taskId
            },
            
            success: function() {
                
                thisTask.closest('tr').remove();
                
                $('#my-notice').text('Task ' + taskId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of task ' + taskId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
